#include <libplayerc++/playerc++.h>
#include <iostream>
#include <fstream>
#include <stdlib.h>
#include <math.h>
#include <vector>
#include <time.h>

using namespace PlayerCc;
using namespace std;

bool CollisionFree(double mapArray[][], double x,double y);
TreeNode nearest( vector <TreeNodes> Nodes, double x, double y);
TreeNode stopping_configuration( vector<TreeNode> Nodes, int Nearest, double x, double y, double pixels[][]);
bool isParent(vector<TreeNodes> Nodes, TreeNode Curr);
vector<TreeNode> FindEnds(vector<TreeNode> Nodes);
vector<TreeNode> FindRoute(vector<TreeNode> Root, vector<TreeNode> Goal);
vector<TreeNode> findShortcut(vector<TreeNode> Start, vector<TreeNode> End);


struct TreeNode{
	double Xval;
	double Yval;
	int parent;
};

bool CollisionFree(double mapArray[][], double x,double y){

	int arrX;
	int arrY;
	x = x + 8.0;
	y = y + 8.0;

	arrX = (37.0 * x);
	arrY = (37.0 * y);
	bool b = 1;

	if(mapArray[arrX][arrY] < 100){
		bool = 0;
		return b;
	}
	return b;

}

int Nearest( vector <TreeNode> Nodes, double x, double y){
//NEAREST - gets nearest node to x,y

	double dist = 999999999;

	int closest;
	TreeNode temp;
	double new_dist;

	for (int i = 0; i < Nodes.size(); i++){
	    temp.Xval = Nodes[i].Xval;
	    temp.Yval = Nodes[i].Yval;

	    new_dist = sqrt(((temp.Xval*temp.Xval)-(x*x))+((temp.Yval*temp.Yval)-(y*y)));

	    if( new_dist < dist){
		dist = sqrt(((temp.Xval*temp.Xval)-(x*x))+((temp.Yval*temp.Yval)-(y*y)));
		closest = i;
		
		
	    }
	}
	return closest;
 
}


TreeNode stopping_configuration( vector<TreeNode> Nodes, int Nearest, double x, double y, double pixels[][]){
	TreeNode StopConfig = new TreeNode;
	
	double Delta = 0.5;
	double Distance = sqrt(((x-Nodes[Nearest].Xval)*(x-Nodes[Nearest].Xval)) + ((y-Node[Nearest].Yval)*(y-Node[Nearest].Yval)));
	
	StopConfig.Xval = Nodes[Nearest].Xval;
	StopConfig.Yval = Nodes[Nearest].Yval;

	double NumPartitions = Distance/Delta;
	double newX, newY;
	bool b;

	for(double i=0; i<=NumPartitions; i++){
		newX = Nodes[Nearest].Xval + (((x-Nodes[Nearest].Xval)/NumPartitions)*i);
		newY = Nodes[Nearest].Yval + (((y-Nodes[Nearest].Yval)/NumPartitions)*i);

		b = CollisionFree(pixels[][],newX,newY);
		if(b){
			StopConfig.Xval = newX;
			StopConfig.Yval = newY;
		}else{
			return StopConfig;
		}
	}

}

bool isParent(vector<TreeNodes> Nodes, TreeNode Curr){
	bool b = 0;
	for(int i = 0; i<Nodes.size(); i++){
		if(Nodes[Nodes[i].parent] == Curr){
			b=1;
			retrurn b;
		}
	}
	return b;
}

vector<TreeNode> FindEnds(vector<TreeNode> Nodes){
	vector<TreeNode> Ends;
	for(int i = 0; i<Nodes.size(); i++){
		if(isParent(Nodes, Nodes[i]) == 0))
			Ends.pushback(Nodes[i])
	}
	
	return Ends;

}

vector<TreeNode> FindRoute(TreeNode Root, TreeNode Goal){
	TreeNode Current = Goal;
	vector<TreeNode> Route;
	Route.push_front(Goal);

	while(current!= Root){
		Route.push_front(Current.parent);
		Current = Current.parent;
	}	

	return Route;
}


vector<TreeNode> findShortcut(vector<TreeNode> Start, vector<TreeNode> End){
	vector<TreeNode> Shortcut;		

	for(int i = Start.size()-1; i>0; i--){
		for(int j = End.size()-1; j>0; j--){
			if(Start[i] == End[j]){
				for(int k = Start.size()-1; k>=i; k--)
					Shortcut.push_back(Start[i]);
				for(int l = j+1; l<End.size(); l++)
					Shortcut.push_back(End[j]);
			}
		}
	}
	
	return Shortcut;

}



double Rand_double(double fMin, double fMax, int i)
{
    srand(i);
    double f = (double)rand() / RAND_MAX;
    return fMin + f * (fMax - fMin);
}



void Forward(PlayerClient &robot,Position2dProxy &pp); // this function moves the robot 1m forward;
void Backwards(PlayerClient &robot,Position2dProxy &pp); // moves the robot 1m backwards;

double GetSensor(PlayerClient &robot,Position2dProxy &pp); // queries the sensor

void Initialize(PlayerClient&robot,Position2dProxy &pp);

char commands[] = { 'F','F','B','F','B','F','F','F','F' };




int main(int argc, char** argv)
{

	PlayerClient robot("127.0.0.1", 6665);
	Position2dProxy pp(&robot,0);
	
	Initialize(robot,pp);

	// your code goes here....

	double pixels[500][500];  //Read in cave.png from png.txt
	char dummy;
	ifstream myfile;
	myfile.open("png.txt");
	for(int i = 0; i < 500; i++){
		for(int j = 0; j < 500; j++){
			if(j==499)
				myfile>>pixels[i][j];
			else{
				myfile>>pixels[i][j];
				myfile>>dummy;
			}
				
			
		}
	}	


	vector<TreeNode> Nodes;  //create vector of nodes

	//read in starting position
	TreeNode Root;
	ifstream startpose;
	startpose.open("startpose.txt");
	startpose>>dummy;
	startpose>>Root.Xval;
	startpose>>Root.Yval;
	
	Nodes.push_back(Root);  //push root onto NODES

	int Iterations = 100;  //Number of nodes created

	//set min and max values
	double x_min = -8;
	double x_max = 8;
	double y_min = -8;
	double y_max = 8;

	for(int i = 1; i<=Iterations; i++){  //creation of Nodes
		double x_rand = Rand_double(x_min,x_max,i);
		double y_rand = Rand_double(y_min,y_max,i*100);

		TreeNode NewNode;
		NewNode.Xval = x_rand;
		NewNode.Yval = y_rand;
		
		int NearestNode = Nearest( Nodes, NewNode.Xval, NewNode.Yval);  //check for nearest node to newly created node
		TreeNode StoppingNode = stopping_configuration( Nodes, NearestNode, NewNode.Xval, NewNode.Yval, pixels[][]); //gets stopping configuration node in case of a collosion with an object

		if((StoppingNode.Xval != Nodes[NearestNode].Xval) && (StoppingNode.Yval != Nodes[NearestNode].Yval)){  //sets parent and new xval/yval according to stopping config
			NewNode.Xval = StoppingNode.Xval;
			NewNode.Yval = StoppingNode.Yval;
			NewNode.Parent = NearestNode;
			Nodes.push_back(NewNode);
		}	

		
	}

	vector<TreeNode> ENDS = FindEnds(Nodes);  //calls FindEnds, which finds all the nodes that do not have children and puts them in ENDS
	vector< vector<TreeNode> > ROUTES; //Initialize Route Vector
	vector<TreeNode> SHORTCUT; //Initialize SHORTCUT;

	for(int i = 0; i<ENDS.size(); i++){  //Finds routes for all end nodes
		ROUTES[i] = findRoute(Root,ENDS[i]);
	}

	for(int i = 0; i<ROUTES.size()-1; i++){  //finds shortcuts between end nodes
		SHORTCUT = findShortcut(Route[i],Route[i+1]);
		if(!SHORTCUT.empty()){
			ROUTES[i+1] = SHORTCUT;
			SHORTCUT.clear();
		}
	}

	for(int i = 0; i<ROUTES.size(); i++){   //EXECUTE ROUTES
		ExecuteRoute(ROUTES[i]); //NEEDS TO IMPLEMENTED*********************************************************************************************
	}

	


	
	

}




